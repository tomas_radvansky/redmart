//
//  MapObject.swift
//  redmart
//
//  Created by Tomas Radvansky on 30/06/2015.
//  Copyright (c) 2015 Radvansky Solutions. All rights reserved.
//

import UIKit
struct MapSize {
    var rowSize:Int
    var columnSize:Int
}
struct Coordinates {
    var row:Int
    var column:Int
}
struct Move {
    var coord:Coordinates
    var value:Int
}

class MapObject: NSObject {
    var size:MapSize!
    var mapData:Array<Array<Int>>!
    
    init(fromFile path:NSURL)
    {
        if let data:NSData = NSData(contentsOfURL: path)
        {
            if let stringData:String = NSString(data: data, encoding: NSUTF8StringEncoding) as? String
            {
                let components = stringData.componentsSeparatedByString("\n")
                let sizeString:String = components[0]
                let sizeComponents = sizeString.componentsSeparatedByString(" ")
                self.size = MapSize(rowSize: sizeComponents[0].toInt()!, columnSize: sizeComponents[1].toInt()!)
                self.mapData = Array<Array<Int>>()
                
                for xvalue in 1...self.size.rowSize
                {
                    let dataComponents = components[xvalue].componentsSeparatedByString(" ")
                    var row:Array<Int> = Array<Int>()
                    for yvalue in 0...self.size.columnSize-1
                    {
                        row.append(dataComponents[yvalue].toInt()!)
                    }
                    self.mapData.append(row)
                }
            }
        }
    }
    
    func getValueFromMap(coord:Coordinates)->Int
    {
        if coord.row<self.size.rowSize-1 && coord.column<self.size.columnSize-1 && coord.row > -1 && coord.column > -1
        {
            return self.mapData[coord.row][coord.column]
        }
        else
        {
            return -1
        }
    }
    
    func getPossibleMoves(coord:Coordinates)->Array<Move>
    {
        var possibleMoves:Array<Move> = []
        let currentValue:Int = self.getValueFromMap(coord)
        if currentValue > -1
        {
            if coord.row<self.size.rowSize-1 && coord.column<self.size.columnSize-1 && coord.row > -1 && coord.column > -1
            {
                
                if coord.column - 1 > -1
                {
                    let newValue:Int = self.getValueFromMap(Coordinates(row: coord.row, column: coord.column-1))
                    if newValue < currentValue
                    {
                        possibleMoves.append(Move(coord: Coordinates(row: coord.row, column: coord.column-1), value: newValue))
                    }
                }
                
                if coord.row - 1 > -1
                {
                    let newValue:Int = self.getValueFromMap(Coordinates(row: coord.row-1, column: coord.column))
                    if newValue < currentValue
                    {
                        possibleMoves.append(Move(coord: Coordinates(row: coord.row-1, column: coord.column), value: newValue))
                    }
                }
                
                if coord.column + 1 < self.size.columnSize-1
                {
                    let newValue:Int = self.getValueFromMap(Coordinates(row: coord.row, column: coord.column+1))
                    if newValue < currentValue
                    {
                        possibleMoves.append(Move(coord: Coordinates(row: coord.row, column: coord.column+1), value: newValue))
                    }
                }
                
                if coord.row + 1 < self.size.rowSize-1
                {
                    let newValue:Int = self.getValueFromMap(Coordinates(row: coord.row+1, column: coord.column))
                    if newValue < currentValue
                    {
                        possibleMoves.append(Move(coord: Coordinates(row: coord.row+1, column: coord.column), value: newValue))
                    }
                }
            }
        }
        //Sort array
        possibleMoves.sort({ $0.value > $1.value })
        
        return possibleMoves
    }
}
