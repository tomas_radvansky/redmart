//
//  ProblemSolver.swift
//  redmart
//
//  Created by Tomas Radvansky on 30/06/2015.
//  Copyright (c) 2015 Radvansky Solutions. All rights reserved.
//

import UIKit

class ProblemSolver: NSObject {
    var mapToSolve:MapObject!
    var acceptedRoutes:Array<Array<Move>> = []
    var currentRoutes:Array<Array<Move>> = []
    var currentRoute:Array<Move> = []
    init (map:MapObject)
    {
        super.init()
        self.mapToSolve = map
    }
    
    func SolveMap()->String
    {
        self.acceptedRoutes = []
        var currentRow:Int = 0
        for row in mapToSolve.mapData
        {
            let maxNumber:Int = maxElement(row)
            let indexOfMaxNumber:Int = find(row, maxNumber)!
            self.currentRoutes = []
            self.currentRoute = []
            self.processCurrentMoves(Coordinates(row: currentRow, column: indexOfMaxNumber))
            self.getAcceptedRoutes()
            currentRow++
        }
        
        return getStringFromRoute(getBestRoute())
    }
    
    func processCurrentMoves(coord:Coordinates)
    {
        var currentMoves:Array<Move> = self.mapToSolve.getPossibleMoves(coord)
        var currentCoord:Coordinates = coord
        if currentMoves.count == 0
        {
            self.currentRoutes.append(self.currentRoute)
            self.currentRoute = []
        }
        else
        {
            for move in currentMoves
            {
                self.currentRoute.append(move)
                self.processCurrentMoves(move.coord)
            }
        }
    }
    
    func getAcceptedRoutes()
    {
        var result:Array<Array<Move>> = []
        var lastRoute:Int = 0
        for route in self.currentRoutes
        {
            if route.count >= lastRoute
            {
                lastRoute = route.count
            }
        }
        for route in self.currentRoutes
        {
            if route.count == lastRoute
            {
                result.append(route)
            }
        }
        //In result i have all accepted routes, now check if their lengh is higher then existing routes
        var lastAcceptedRoute:Int = 0
        for route in self.acceptedRoutes
        {
            if route.count >= lastAcceptedRoute
            {
                lastAcceptedRoute = route.count
            }
        }
        
        var newAcceptedRoutes:Array<Array<Move>> = []
        if lastRoute >= lastAcceptedRoute
        {
            for route in self.acceptedRoutes
            {
                if route.count >= lastRoute
                {
                    newAcceptedRoutes.append(route)
                }
            }
            self.acceptedRoutes = newAcceptedRoutes
            self.acceptedRoutes += result
        }
    }
    
    func getBestRoute()->Array<Move>
    {
        var bestRoute:Array<Move> = []
        for route in self.acceptedRoutes
        {
            let firstValue:Int = route[0].value
            let lastValue:Int = route[route.count-1].value
            let slope:Int = firstValue - lastValue
            
            var oldSlope:Int = 0
            if bestRoute.count > 0
            {
                let oldFirstValue:Int = bestRoute[0].value
                let oldLastValue:Int = bestRoute[bestRoute.count-1].value
                oldSlope = oldFirstValue - oldLastValue
            }
            
            if slope > oldSlope
            {
                bestRoute = route
            }
        }
        return bestRoute
    }
    
    func getStringFromRoute(route:Array<Move>)->String
    {
        var lenght:String = "\(route.count)"
        let firstValue:Int = route[0].value
        let lastValue:Int = route[route.count-1].value
        let slope:Int = firstValue - lastValue
        var slopeStr:String = "\(slope)"
        return lenght+slopeStr+"@redmart.com"
    }
}
