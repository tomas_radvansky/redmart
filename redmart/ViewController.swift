//
//  ViewController.swift
//  redmart
//
//  Created by Tomas Radvansky on 30/06/2015.
//  Copyright (c) 2015 Radvansky Solutions. All rights reserved.
//

import UIKit
import AFNetworking
import MRProgress

class ViewController: UIViewController {
    
    @IBOutlet weak var mapSizeLabel: UILabel!
    @IBOutlet weak var emailButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Load map
        let configuration:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let manager:AFURLSessionManager = AFURLSessionManager(sessionConfiguration: configuration)
        var progress:NSProgress?
        let fileman:NSFileManager = NSFileManager.defaultManager()
        let documentsDir:NSURL = fileman.URLForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomain: NSSearchPathDomainMask.UserDomainMask, appropriateForURL: nil, create: false, error: nil)!
        let filepath:String = documentsDir.URLByAppendingPathComponent("map.txt").path!
        if fileman.fileExistsAtPath(filepath) == true
        {
            let overlayView:MRProgressOverlayView = MRProgressOverlayView.showOverlayAddedTo(self.view, animated: true)
            let mapObj:MapObject = MapObject(fromFile: documentsDir.URLByAppendingPathComponent("map.txt"))
            let startTime:NSDate = NSDate()
            self.emailButton.setTitle(ProblemSolver(map: mapObj).SolveMap(), forState: UIControlState.Normal)
            mapSizeLabel.text = "time elapsed: \(startTime.timeIntervalSinceNow * -1)s"
            overlayView.dismiss(true)
        }
        else
        {
            if let url:NSURL = NSURL(string: "http://s3-ap-southeast-1.amazonaws.com/geeks.redmart.com/coding-problems/map.txt")
            {
                let overlayView:MRProgressOverlayView = MRProgressOverlayView.showOverlayAddedTo(self.view, animated: true)
                let request:NSURLRequest = NSURLRequest(URL: url)
                let downloadTask:NSURLSessionDownloadTask = manager.downloadTaskWithRequest(request, progress: &progress, destination: { (url:NSURL!, response:NSURLResponse!) -> NSURL! in
                    return documentsDir.URLByAppendingPathComponent(response.suggestedFilename!)
                    }, completionHandler: { (response:NSURLResponse!, filepath:NSURL!, err:NSError!) -> Void in
                        println("File downloaded to: \(filepath)")
                        let mapObj:MapObject = MapObject(fromFile: documentsDir.URLByAppendingPathComponent("map.txt"))
                        let startTime:NSDate = NSDate()
                        self.emailButton.setTitle(ProblemSolver(map: mapObj).SolveMap(), forState: UIControlState.Normal)
                        self.mapSizeLabel.text = "time elapsed: \(startTime.timeIntervalSinceNow * -1)s"
                        overlayView.dismiss(true)
                })
                downloadTask.resume()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func emailBtnClicked(sender: AnyObject) {
        
    }
    
}

